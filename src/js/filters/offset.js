(function () {'use strict';
    angular.module('honeycomrade.filter').filter('offset', [function(){
　　　     return function (array, offset) {
            if (!!array) {
                offset = parseInt(offset);
                return array.slice(offset);
            } else {
                return [];
            }
        }
    }]);
})();
