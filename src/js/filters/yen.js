(function () {'use strict';
	angular.module('honeycomrade.filter').filter('yen', ['$filter', '$sce', function($filter, $sce){
　　　		return function (number, fractionSize) {
			var yen = isFinite(number) ? '&yen' + $filter('number')(number, fractionSize) : '';
			
    		return $sce.trustAsHtml(yen);
    	};
	}]);
})();
