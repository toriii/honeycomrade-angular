module.exports = function(grunt) {
  grunt.initConfig({
  	concat: {
    	js: {
    		src: ['src/js/*.js', 'src/js/*/*.js'],
        	dest: 'dist/js/huneycomrade.js'
    	},
    	css: {
    		src: ['src/css/*.css'],
        	dest: 'dist/css/huneycomrade.css'
    	}
    },
    cssmin: {
  		compress: {
  			files: {'dist/css/huneycomrade.min.css': 'dist/css/huneycomrade.css'}
  		}
  	},
    uglify: {
    	js: {
    		src: 'dist/js/huneycomrade.js',
        	dest: 'dist/js/huneycomrade.min.js'
    	}
    }
  });
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.registerTask('default', ['concat']);
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.registerTask('default', ['uglify']);
};