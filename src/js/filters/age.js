(function () {'use strict';
	angular.module('honeycomrade.filter').filter('age', ['$filter', function($filter){
　　　		return function (birthday) {
			birthday = $filter('date')(birthday, 'yyyyMMdd');
			var today = $filter('date')(new Date(), 'yyyyMMdd');
			var age = parseInt((today - birthday) / 10000);

    		return age;
    	};
	}]);
})();
