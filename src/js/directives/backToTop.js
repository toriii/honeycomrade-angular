(function () {'use strict';
    angular.module('honeycomrade.directive').directive('backToTop', ['$document', '$window', '$timeout', '$anchorScroll', function($document, $window, $timeout, $anchorScroll){
	  return {
            link : function(scope, element, attrs) {
                var body = $document.find('body');
                var fps = 10; // 30fps
                var frameTime = 1000 / fps;
                if (!element.html()) {
                    var template = [
                        '<div class="honeycomrade-back-to-top">',
                            '<div>▲</div>',
                        '</div>'
                    ].join('');
                    element.append(template);
                }
                element.css('cursor', 'pointer');
                element.click(function(){
                    $('html, body').animate({scrollTop: 0}, 'fast');
                });
                var isScrollChecked = false;
                element.hide();
                scrolleCheck();
                angular.element($window).bind("scroll", function(e) {
                   scrolleCheck();
                });
                function scrolleCheck() {
                    if (!isScrollChecked) {
                        isScrollChecked = true;
                        $timeout(function(){
                            if (angular.element($window).scrollTop() > 100) {
                                element.fadeIn('slow');
                            } else {
                                element.fadeOut('slow');
                            }
                            isScrollChecked = false;
                        }, frameTime);
                   }
                }
            }
        };
    }]);
})();