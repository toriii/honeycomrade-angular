(function () {'use strict';
	angular.module('honeycomrade.filter').filter('htmlEscape', ['$sce', function($sce){
　　　		return function (text) {
    		return text ? text.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/&/, '&amp;') : '';
    	};
	}]);
})();
