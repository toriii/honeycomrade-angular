(function () {'use strict';
	angular.module('honeycomrade', [
		'honeycomrade.directive',
		'honeycomrade.filter',
		'honeycomrade.value'
	]);
	angular.module('honeycomrade.directive', []);
	angular.module('honeycomrade.filter', []);
	angular.module('honeycomrade.value', []);
})();
(function () {'use strict';
    angular.module('honeycomrade.directive').directive('backToTop', ['$document', '$window', '$timeout', '$anchorScroll', function($document, $window, $timeout, $anchorScroll){
	  return {
            link : function(scope, element, attrs) {
                var body = $document.find('body');
                var fps = 10; // 30fps
                var frameTime = 1000 / fps;
                if (!element.html()) {
                    var template = [
                        '<div class="honeycomrade-back-to-top">',
                            '<div>▲</div>',
                        '</div>'
                    ].join('');
                    element.append(template);
                }
                element.css('cursor', 'pointer');
                element.click(function(){
                    $('html, body').animate({scrollTop: 0}, 'fast');
                });
                var isScrollChecked = false;
                element.hide();
                scrolleCheck();
                angular.element($window).bind("scroll", function(e) {
                   scrolleCheck();
                });
                function scrolleCheck() {
                    if (!isScrollChecked) {
                        isScrollChecked = true;
                        $timeout(function(){
                            if (angular.element($window).scrollTop() > 100) {
                                element.fadeIn('slow');
                            } else {
                                element.fadeOut('slow');
                            }
                            isScrollChecked = false;
                        }, frameTime);
                   }
                }
            }
        };
    }]);
})();
(function () {'use strict';
    angular.module('honeycomrade.directive').directive('lightbox', ['$document', function($document){
        return {
            link : function(scope, element, attrs) {
            	element.addClass('honeycomrade-lightbox-image');
            	var body = $document.find('body');
            	element.click(function(){
            		var lightboxTemplate = [
            			'<div class="honeycomrade-lightbox">',
            				'<div class="honeycomrade-lightbox-backdrop"></div>',
            				'<div class="honeycomrade-lightbox-content">',
            					'<div class="honeycomrade-lightbox-btn">×</div>',
            					'<img src="'+ attrs.ngSrc +'">',
            				'</div>',
            			'</div>'
            		].join('');
            		body.append(lightboxTemplate);
            		var lightbox = body.find('.honeycomrade-lightbox');
            		var content = lightbox.find('.honeycomrade-lightbox-content');
            		var img = content.find('img');
            		content.css('honeycomrade-lightbox-content-loading');
            		img.bind('load', function(){
            			content.width(img.width())
            				.height(img.height())
            				.removeClass('honeycomrade-lightbox-content-loading');
            			content.css('display', 'none');
            			content.fadeIn();
            		});
            		body.find('.honeycomrade-lightbox-backdrop, .honeycomrade-lightbox-btn').click(function(){
            			lightbox.fadeOut('normal', function(){
            				lightbox.remove();
            			});
            		});
            	});
            }
        };
    }]);
})();
(function () {'use strict';
    angular.module('honeycomrade.directive').directive('loading', [function(){
        return {
        	restrict: 'E',
        	replace: true,
            template: [
            	'<div class="honeycomrade-spinner">',
					'<div class="spinner-container container1">',
		    			'<div class="circle1"></div>',
		    			'<div class="circle2"></div>',
		    			'<div class="circle3"></div>',
		    			'<div class="circle4"></div>',
	  				'</div>',
					'<div class="spinner-container container2">',
						'<div class="circle1"></div>',
						'<div class="circle2"></div>',
						'<div class="circle3"></div>',
						'<div class="circle4"></div>',
					'</div>',
					'<div class="spinner-container container3">',
						'<div class="circle1"></div>',
						'<div class="circle2"></div>',
						'<div class="circle3"></div>',
						'<div class="circle4"></div>',
					'</div>',
				'</div>'
			].join(''),
            link : function(scope, element, attrs) {
            	var height = !!attrs.height ? attrs.height : 100;
            	var size = !!attrs.size ? attrs.size : 20;
            	var margin = (height - size) / 2;

            	element
            		.width(size)
	            	.height(size)
	            	.css('margin-top', margin)
	            	.css('margin-bottom', margin)
            	;
            }
        };
    }]);
})();

(function () {'use strict';
	angular.module('honeycomrade.directive').directive('thumbnail', ['$compile', function($compile){
		return {
                  link : function(scope, element, attrs) {
            	var size = parseInt(attrs.thumbnail);
            	if (!size) {
            		size = element.parent().width();
            	}
            	element.css('display', 'none');
            	element.bind("load" , function(e){
            		element.css('position', 'relative');
            		if (element.width() > element.height()) {
            			element.css('height', size);
            			var right = (element.width() - size) / 2;
            			element.css('right', right);
            		} else {
            			element.css('width', size);
            			var bottom = (element.height() - size) / 2;
            			element.css('bottom', bottom);
            		}
            		element.css('display', 'inline');
            		element.wrap('<div class="thumbnail" style="width:' + size +';height:' + size + '"></div>');
            	});
            }
        };
	}]);
})();
(function () {'use strict';
	angular.module('honeycomrade.filter').filter('age', ['$filter', function($filter){
　　　		return function (birthday) {
			birthday = $filter('date')(birthday, 'yyyyMMdd');
			var today = $filter('date')(new Date(), 'yyyyMMdd');
			var age = parseInt((today - birthday) / 10000);

    		return age;
    	};
	}]);
})();

(function () {'use strict';
	angular.module('honeycomrade.filter').filter('gender', ['gender', function(gender){
　　　		return function (value, type) {
			if (!type) {
				type = 1;
			}
			if (angular.isDefined(gender[value])) {
				value = gender[type][value];
			}
    		return value;
    	};
	}]);
})();

(function () {'use strict';
	angular.module('honeycomrade.filter').filter('htmlEscape', ['$sce', function($sce){
　　　		return function (text) {
    		return text ? text.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/&/, '&amp;') : '';
    	};
	}]);
})();

(function () {'use strict';
	angular.module('honeycomrade.filter').filter('nl2br', ['$sce', function($sce){
　　　		return function (text) {
    		return text ? $sce.trustAsHtml(text.replace(/\n/g, '<br />')) : '';
    	};
	}]);
})();

(function () {'use strict';
    angular.module('honeycomrade.filter').filter('offset', [function(){
　　　     return function (array, offset) {
            if (!!array) {
                offset = parseInt(offset);
                return array.slice(offset);
            } else {
                return [];
            }
        }
    }]);
})();

(function () {'use strict';
	angular.module('honeycomrade.filter').filter('percentage', [function(){
　　　		return function (number, fractionSize) {
    		if (isFinite(number)) {
    			number = number * 100;
    			fractionSize = isFinite(fractionSize) ? fractionSize : 3;
    			if (fractionSize > 0) {
					var digit = Math.pow(10, fractionSize);
					number = number * digit;
    			}
    			number = Math.round(number);
    			if (fractionSize > 0) {
    				number = number / digit;
    			}
    			number = number + '%';
    		}

    		return number;
    	};
	}]);
})();

(function () {'use strict';
	angular.module('honeycomrade.filter').filter('prefecture', ['prefecture', function(prefecture){
　　　		return function (value) {
			if (angular.isDefined(prefecture[value])) {
				value = prefecture[value];
			}
    		return value;
    	};
	}]);
})();

(function () {'use strict';
	angular.module('honeycomrade.filter').filter('strimwidth', ['$filter', function($filter){
　　　		return function (text, width, trimmarker) {
			if (!text) {
				return;
			}
			if (isFinite(width) && text.length > width) {
				trimmarker = !!trimmarker ? trimmarker : '...';
				text = $filter('limitTo')(text, width) + trimmarker;
			}

    		return text;
    	};
	}]);
})();

(function () {'use strict';
	angular.module('honeycomrade.filter').filter('yen', ['$filter', '$sce', function($filter, $sce){
　　　		return function (number, fractionSize) {
			var yen = isFinite(number) ? '&yen' + $filter('number')(number, fractionSize) : '';
			
    		return $sce.trustAsHtml(yen);
    	};
	}]);
})();

(function () {'use strict';
	angular.module('honeycomrade.value').value('gender', {
		1: {
			1: '男性',
			2: '女性'
		},
		2: {
			1: '男',
			2: '女'
		},
		3: {
			1: 'おとこ',
			2: 'おんな'
		},
		4: {
			1: '♂',
			2: '♀'
		}
	});
})();
(function () {'use strict';
	angular.module('honeycomrade.value').value('prefecture', {
		1: '北海道',
		2: '青森県',
		3: '岩手県',
		4: '宮城県',
		5: '秋田県',
		6: '山形県',
		7: '福島県',
		8: '茨城県',
		9: '栃木県',
		10: '群馬県',
		11: '埼玉県',
		12: '千葉県',
		13: '東京都',
		14: '神奈川県',
		15: '新潟県',
		16: '富山県',
		17: '石川県',
		18: '福井県',
		19: '山梨県',
		20: '長野県',
		21: '岐阜県',
		22: '静岡県',
		23: '愛知県',
		24: '三重県',
		25: '滋賀県',
		26: '京都府',
		27: '大阪府',
		28: '兵庫県',
		29: '奈良県',
		30: '和歌山県',
		31: '鳥取県',
		32: '島根県',
		33: '岡山県',
		34: '広島県',
		35: '山口県',
		36: '徳島県',
		37: '香川県',
		38: '愛媛県',
		39: '高知県',
		40: '福岡県',
		41: '佐賀県',
		42: '長崎県',
		43: '熊本県',
		44: '大分県',
		45: '宮崎県',
		46: '鹿児島県',
		47: '沖縄県'
	});
})();