(function () {'use strict';
	angular.module('honeycomrade.value').value('gender', {
		1: {
			1: '男性',
			2: '女性'
		},
		2: {
			1: '男',
			2: '女'
		},
		3: {
			1: 'おとこ',
			2: 'おんな'
		},
		4: {
			1: '♂',
			2: '♀'
		}
	});
})();