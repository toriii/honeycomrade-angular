(function () {'use strict';
    angular.module('honeycomrade.directive').directive('loading', [function(){
        return {
        	restrict: 'E',
        	replace: true,
            template: [
            	'<div class="honeycomrade-spinner">',
					'<div class="spinner-container container1">',
		    			'<div class="circle1"></div>',
		    			'<div class="circle2"></div>',
		    			'<div class="circle3"></div>',
		    			'<div class="circle4"></div>',
	  				'</div>',
					'<div class="spinner-container container2">',
						'<div class="circle1"></div>',
						'<div class="circle2"></div>',
						'<div class="circle3"></div>',
						'<div class="circle4"></div>',
					'</div>',
					'<div class="spinner-container container3">',
						'<div class="circle1"></div>',
						'<div class="circle2"></div>',
						'<div class="circle3"></div>',
						'<div class="circle4"></div>',
					'</div>',
				'</div>'
			].join(''),
            link : function(scope, element, attrs) {
            	var height = !!attrs.height ? attrs.height : 100;
            	var size = !!attrs.size ? attrs.size : 20;
            	var margin = (height - size) / 2;

            	element
            		.width(size)
	            	.height(size)
	            	.css('margin-top', margin)
	            	.css('margin-bottom', margin)
            	;
            }
        };
    }]);
})();
