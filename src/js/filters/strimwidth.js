(function () {'use strict';
	angular.module('honeycomrade.filter').filter('strimwidth', ['$filter', function($filter){
　　　		return function (text, width, trimmarker) {
			if (!text) {
				return;
			}
			if (isFinite(width) && text.length > width) {
				trimmarker = !!trimmarker ? trimmarker : '...';
				text = $filter('limitTo')(text, width) + trimmarker;
			}

    		return text;
    	};
	}]);
})();
