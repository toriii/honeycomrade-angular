(function () {'use strict';
	angular.module('honeycomrade.filter').filter('percentage', [function(){
　　　		return function (number, fractionSize) {
    		if (isFinite(number)) {
    			number = number * 100;
    			fractionSize = isFinite(fractionSize) ? fractionSize : 3;
    			if (fractionSize > 0) {
					var digit = Math.pow(10, fractionSize);
					number = number * digit;
    			}
    			number = Math.round(number);
    			if (fractionSize > 0) {
    				number = number / digit;
    			}
    			number = number + '%';
    		}

    		return number;
    	};
	}]);
})();
