(function () {'use strict';
	angular.module('honeycomrade.filter').filter('prefecture', ['prefecture', function(prefecture){
　　　		return function (value) {
			if (angular.isDefined(prefecture[value])) {
				value = prefecture[value];
			}
    		return value;
    	};
	}]);
})();
