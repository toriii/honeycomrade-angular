(function () {'use strict';
    angular.module('honeycomrade.directive').directive('lightbox', ['$document', function($document){
        return {
            link : function(scope, element, attrs) {
            	element.addClass('honeycomrade-lightbox-image');
            	var body = $document.find('body');
            	element.click(function(){
            		var lightboxTemplate = [
            			'<div class="honeycomrade-lightbox">',
            				'<div class="honeycomrade-lightbox-backdrop"></div>',
            				'<div class="honeycomrade-lightbox-content">',
            					'<div class="honeycomrade-lightbox-btn">×</div>',
            					'<img src="'+ attrs.ngSrc +'">',
            				'</div>',
            			'</div>'
            		].join('');
            		body.append(lightboxTemplate);
            		var lightbox = body.find('.honeycomrade-lightbox');
            		var content = lightbox.find('.honeycomrade-lightbox-content');
            		var img = content.find('img');
            		content.css('honeycomrade-lightbox-content-loading');
            		img.bind('load', function(){
            			content.width(img.width())
            				.height(img.height())
            				.removeClass('honeycomrade-lightbox-content-loading');
            			content.css('display', 'none');
            			content.fadeIn();
            		});
            		body.find('.honeycomrade-lightbox-backdrop, .honeycomrade-lightbox-btn').click(function(){
            			lightbox.fadeOut('normal', function(){
            				lightbox.remove();
            			});
            		});
            	});
            }
        };
    }]);
})();