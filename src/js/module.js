(function () {'use strict';
	angular.module('honeycomrade', [
		'honeycomrade.directive',
		'honeycomrade.filter',
		'honeycomrade.value'
	]);
	angular.module('honeycomrade.directive', []);
	angular.module('honeycomrade.filter', []);
	angular.module('honeycomrade.value', []);
})();