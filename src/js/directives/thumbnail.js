(function () {'use strict';
	angular.module('honeycomrade.directive').directive('thumbnail', ['$compile', function($compile){
		return {
                  link : function(scope, element, attrs) {
            	var size = parseInt(attrs.thumbnail);
            	if (!size) {
            		size = element.parent().width();
            	}
            	element.css('display', 'none');
            	element.bind("load" , function(e){
            		element.css('position', 'relative');
            		if (element.width() > element.height()) {
            			element.css('height', size);
            			var right = (element.width() - size) / 2;
            			element.css('right', right);
            		} else {
            			element.css('width', size);
            			var bottom = (element.height() - size) / 2;
            			element.css('bottom', bottom);
            		}
            		element.css('display', 'inline');
            		element.wrap('<div class="thumbnail" style="width:' + size +';height:' + size + '"></div>');
            	});
            }
        };
	}]);
})();