(function () {'use strict';
	angular.module('honeycomrade.filter').filter('gender', ['gender', function(gender){
　　　		return function (value, type) {
			if (!type) {
				type = 1;
			}
			if (angular.isDefined(gender[value])) {
				value = gender[type][value];
			}
    		return value;
    	};
	}]);
})();
